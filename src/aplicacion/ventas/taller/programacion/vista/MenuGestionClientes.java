package aplicacion.ventas.taller.programacion.vista;

import aplicacion.ventas.taler.programacion.controler.GestionCliente;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;


public class MenuGestionClientes {
    //NOTA: el metodo 'nextLine()' captura toda la linea incluyendo espacios
    Scanner entrada = new Scanner(System.in);
    
    public int menuClientes(){
        
        System.out.println("\t\t   GESTION CLIENTES");
        System.out.println("--------------------------------------------------");
        System.out.println("");
        System.out.println("\t\t1. Registrar cliente");
        System.out.println("\t\t2. Actualizar cliente");
        System.out.println("\t\t3. Eliminar cliente");
        System.out.println("\t\t4. Buscar cliente");
        System.out.println("\t\t5. Listar clientes");
        System.out.println("\t\t0. Regresar menu principal");
        

        
        System.out.print("Digite una opcion: ");
        int op = entrada.nextInt();
        System.out.println("");
        
        
        switch(op){
            case 1: registrarCliente(); break;
            case 2: actualizarCliente(); break;
            case 3: eliminarCliente(); break;
            case 4: buscarCliente(); break;
            case 5: listarClientes(); break;
            case 0: MenuPrincipalVista mp = new MenuPrincipalVista();
                    mp.menu();
                    break;
            default: menuClientes(); break;
            
        }
        
        System.out.println("--------------------------------------------------");
        return op;
    }
    
    
    public void registrarCliente(){
        this.entrada.nextLine();
        
        System.out.println("\t\t   REGISTRAR CLIENTE");
        System.out.println("\t\t   -----------------");
        System.out.println("");
        
        System.out.print("Ingrese codigo: ");
        String id = this.entrada.nextLine();
        
        System.out.print("Ingrese nombre: ");
        String nombre = this.entrada.nextLine();
        
        System.out.print("Ingrese numero documento: ");
        String numeroDoc = this.entrada.nextLine();
        
        System.out.print("Ingrese direccion: ");
        String direccion = this.entrada.nextLine();
        
        System.out.println("");
        
        GestionCliente gc = new GestionCliente();
        String mensaje = gc.insertarINDB(id, nombre, numeroDoc, direccion);
        System.out.println(mensaje);
        
        System.out.println("");
        System.out.println("Presione enter para continuar .....");
        this.entrada.nextLine();
        
        
    }
        
    public void actualizarCliente(){
        this.entrada.nextLine();
        
        System.out.println("\t\t     ACTUALIZACION  ");
        System.out.println("\t\t   -----------------");
        System.out.println("");
        
        System.out.print("Ingrese el codigo de quien desea MODIFICAR: ");
        String id = this.entrada.nextLine();                
        
        System.out.print("Ingrese nombre: ");
        String nombre = this.entrada.nextLine();
        
        System.out.print("Ingrese el numero de documento: ");
        String numDoc = this.entrada.nextLine();
        
        System.out.print("Ingrese la direccion: ");
        String direccion = this.entrada.nextLine();
        
        System.out.println("");

        GestionCliente gc = new GestionCliente();
        String mensaje = gc.actualizarINDB(id, nombre, numDoc, direccion);
        System.out.println(mensaje);
        
        System.out.println("");
        System.out.println("Presione enter para continuar .....");
        this.entrada.nextLine();
        
    }
    
    public void eliminarCliente(){
        this.entrada.nextLine();
        
        System.out.println("\t\t   ELIMINAR  CLIENTE");
        System.out.println("\t\t   -----------------");
        System.out.println("");
        
        System.out.print("Ingrese el codigo de quien desea ELIMINAR: ");
        String id = this.entrada.nextLine();
        
        System.out.println("");
        
        GestionCliente gc = new GestionCliente();
        String mensaje = gc.elminarINDB(id);
        System.out.println(mensaje);
        
        System.out.println("");
        System.out.println("Presione enter para continuar .....");
        this.entrada.nextLine();
        
    }
    
    public void buscarCliente(){
        String codigo, nombre, documento, direccion;
        this.entrada.nextLine();
        
        System.out.println("\t\t    BUSCAR  CLIENTE ");
        System.out.println("\t\t   -----------------");
        System.out.println("");
        
        System.out.print("Ingrese el codigo de quien desea BUSCAR: ");
        String id = this.entrada.nextLine();
        
        System.out.println("");
        
        mostrarClientesCabezaera();

        try {
            GestionCliente gc = new GestionCliente();
            ResultSet rs = gc.buscarINDB(id);
            if(rs.next()){
                codigo = rs.getString(1);
                nombre = rs.getString(2);
                documento = rs.getString(3);
                direccion = rs.getString(4);
                
                mostrarClienteDetalle(codigo, nombre, documento, direccion);
            }else{
                System.out.println("");
                System.out.println("NO EXISTE EL CLIENTE BUSCADO...");
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        System.out.println("");
        System.out.println("Presione enter para continuar .....");
        this.entrada.nextLine();
    }
    
    public void listarClientes(){
        String codigo, nombre, documento, direccion;
        this.entrada.nextLine();
        
        System.out.println("\t\t   LISTADO  CLIENTES");
                
        mostrarClientesCabezaera();
        
        try {
            GestionCliente gc = new GestionCliente();
            ResultSet rs = gc.listarINDB();
            while(rs.next()){
                codigo = rs.getString(1);
                nombre = rs.getString(2);
                documento = rs.getString(3);
                direccion = rs.getString(4);
                
                mostrarClienteDetalle(codigo, nombre, documento, direccion);
            }
            if(rs == null){
                System.out.println("");
                System.out.println("NO EXISTEN  CLIENTES EN LA BD...");
            } else {
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        System.out.println("");
        System.out.println("Presione enter para continuar .....");
        this.entrada.nextLine();
    }
    
    
    public void mostrarClientesCabezaera(){
        System.out.println("------------------------------------------------------------------");
        System.out.println("CODIGO       NOMBRES Y APELLIDOS        DOCUMENTO        DIRECCION");
        System.out.println("------------------------------------------------------------------");
    }
    //Metodo para alinear los items en la consola
    public void mostrarClienteDetalle(String id, String nombre, String documento, String direccion){        
        
        for(int x = id.length(); x < 13; x++){
            id = id + " ";
        }
        
        for(int x = nombre.length(); x < 27; x++){
            nombre = nombre + " ";
            
        }
        
        for(int x = documento.length(); x < 17; x++){
            documento = documento + " ";
        }
        
        System.out.println(id +  nombre + documento + direccion);
    }
    
    
}
