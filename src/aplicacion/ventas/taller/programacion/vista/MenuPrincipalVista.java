
package aplicacion.ventas.taller.programacion.vista;

import java.util.Scanner;


public class MenuPrincipalVista {

    Scanner entrada = new Scanner(System.in);
    
    public void bienvenida(){
        
        System.out.println("**************************************************");
        System.out.println("*                  BIENVENIDO                    *");
        System.out.println("**************************************************");

    }
    
    public int menu(){
        
        System.out.println("");
        System.out.println("\t\t   MENU PRINCIPAL");
        System.out.println("____________________________________________________");
        System.out.println("");        
        System.out.println("\t\t1. Emitir Venta");
        System.out.println("\t\t2. Gestionar Productos");
        System.out.println("\t\t3. Gestionar Clientes");
        System.out.println("\t\t0. Salir");
                
        System.out.print("Digite una opcion: ");
        int op = entrada.nextInt();
        System.out.println("");
        
        switch(op){
            case 1: /* Falta implementar */break;
            case 2: /* Falta implementar */break;
            case 3: MenuGestionClientes gc = new MenuGestionClientes();
                    int opCliente = -1;
                    System.out.println("--------------------------------------------------");
                    while(opCliente != 0){
                       opCliente = gc.menuClientes();
                    }
                    break;
            case 0: salida(); break;
            default: menu();
        }
        
        return op;
    }
    
    public void salida(){
        
        System.out.println("**************************************************");
        System.out.println("*              FIN DE LA APLICACION              *");
        System.out.println("**************************************************");
        
    }

    
}

