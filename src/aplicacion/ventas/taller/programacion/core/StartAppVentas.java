
package aplicacion.ventas.taller.programacion.core;

import aplicacion.ventas.taller.programacion.vista.MenuPrincipalVista;


public class StartAppVentas {

    
    public static void main(String[] args) {
        MenuPrincipalVista mv = new  MenuPrincipalVista();
        mv.bienvenida();        
        mv.menu();                
    }
    
}
