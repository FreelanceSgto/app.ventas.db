
package aplicacion.ventas.taler.programacion.controler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class GestionCliente {

    final String sqlInsertCliente  = "INSERT INTO cliente VALUES(?, ?, ?, ?);"; 
    final String sqlUpdateCliente  = "UPDATE cliente SET nombre = ?, direccion = ?, numero_documento = ?  WHERE id_cliente = ?;";
    final String sqlDeleteCliente  = "DELETE FROM cliente WHERE id_cliente = ?;";
    final String sqlSelectClientes = "SELECT id_cliente, nombre, numero_documento, direccion FROM cliente;";
    final String sqlSelectCliente  = "SELECT id_cliente, nombre, numero_documento, direccion FROM cliente WHERE id_cliente = ?;";
    
    public String insertarINDB(String id, String nombre, String numeroDoc, String direccion){        
        String mensaje;
        ConexionDB cDB = new ConexionDB();
        Connection c = null;
        try {
            c = cDB.getConection();
            PreparedStatement pst = c.prepareCall(this.sqlInsertCliente);
            pst.setString(1, id);
            pst.setString(2, nombre);
            pst.setString(3, direccion);
            pst.setString(4, numeroDoc);
            pst.executeUpdate();
            
            pst.close();
            c.close();
            
            mensaje = "Exito!! Se registraron los datos correctanmente";
            
        } catch (SQLException e) {
            mensaje = "Error!! no se concreto el registro";
            e.printStackTrace();            
        }
        return mensaje;
    }

    public String actualizarINDB(String id, String nombre, String numeroDoc, String direccion){
        String mensaje;
        ConexionDB cDB = new ConexionDB();
        Connection c = null;
        try {            
            c = cDB.getConection();
            PreparedStatement pst = c.prepareCall(this.sqlUpdateCliente);
            pst.setString(1, nombre);
            pst.setString(2, direccion);
            pst.setString(3, numeroDoc);
            pst.setString(4, id);
            pst.executeUpdate();
            
            pst.close();
            c.close();
            mensaje = "Exito!! Se actualizaron los datos correctamente";
            
        } catch (SQLException e) {
            mensaje = "Error!! no se concreto la actualización";
            e.printStackTrace();            
        }
        
        return mensaje;        
    }

    public String elminarINDB(String id){
        String mensaje;
        ConexionDB cDB = new ConexionDB();
        Connection c = null;
        try {
            
            c = cDB.getConection();
            PreparedStatement pst = c.prepareCall(this.sqlDeleteCliente);
            pst.setString(1, id);            
            pst.executeUpdate();
            
            pst.close();
            c.close();
            mensaje = "Exito!! Se eliminarion los datos correctamente";
            
        } catch (SQLException e) {
            mensaje = "Error!! No se concreto la eliminacion";
            e.printStackTrace();                        
        }
        return mensaje;
    }
    
    public ResultSet buscarINDB(String id){
        
        ConexionDB cDB = new ConexionDB();
        Connection c = null;
        ResultSet rs = null;
        try {
            c = cDB.getConection();
            PreparedStatement pst = c.prepareCall(this.sqlSelectCliente);
            pst.setString(1, id);
            rs = pst.executeQuery();

            //rs.close();
            //pst.close();
            c.close();
                        
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }
    
    public ResultSet listarINDB(){
        ConexionDB cDB = new ConexionDB();
        Connection c = null;
        ResultSet rs = null;
        try {            
            c = cDB.getConection();
            PreparedStatement pst = c.prepareCall(this.sqlSelectClientes);
            rs = pst.executeQuery();            
            //pst.close();
            c.close();            
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }
    
}
