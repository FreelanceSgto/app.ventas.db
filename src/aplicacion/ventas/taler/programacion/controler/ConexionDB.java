/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion.ventas.taler.programacion.controler;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author tonny
 */
public class ConexionDB {
    final String dbUrl = "jdbc:postgresql://localhost:5432/";
    final String dbName = "db_ventas";
    final String dbUser = "postgres";
    final String dbPassword="postgres";
    final String dbDriver="org.postgresql.Driver";
    
    public Connection getConection(){
        Connection c = null;
        try {
            Class.forName(dbDriver);
            c = DriverManager.getConnection(dbUrl + dbName, dbUser, dbPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }
    
    
    
}
